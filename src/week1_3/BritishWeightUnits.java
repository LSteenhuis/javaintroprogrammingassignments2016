/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class BritishWeightUnits {
    private final int pounds;
    private final int ounces;
    private final int grams;

    /**
     * constructs with relevant coins.
     * @param pounds pounds
     * @param ounces ounces
     * @param grams grams
     */
    public BritishWeightUnits(final int pounds, final int ounces, final int grams) {
        this.pounds = pounds;
        this.ounces = ounces;
        this.grams = grams;
    }

    /**
     * returns the pounds.
     * @return pounds
     */
    public final int getPounds() {
        return pounds;
    }

    /**
     * returns the ounces.
     * @return ounces
     */
    public final int getOunces() {
        return ounces;
    }

    /**
     * returns the grams.
     * @return grams
     */
    public final int getGrams() {
        return grams;
    }

    @Override
    public final String toString() {
        return "BritishUnitsCoins{" + "pounds=" + pounds + ", ounces=" + ounces + ", grams=" + grams + '}';
    }

}
