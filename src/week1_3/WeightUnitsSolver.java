/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {
    /**
     * main to be used for testing purposes.
     * @param args arguments
     */
    public static void main(final String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        wus.convertFromGrams(25369);
    }

    /**
     * will return the number of Pounds, Ounces and Grams represented by this quantity of grams,
     * encapsulated in a BritishWeightUnits object.
     * pound = 484 gram
     * ounce = 28 gram
     *
     * @param grams grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is
     */
    public final BritishWeightUnits convertFromGrams(final int grams) {
        if (grams < 0) {
            throw new IllegalArgumentException("Please enter a number that is positive. " + grams + " is below 0.");
        } else {
        int pounds = 0;
        int ounces = 0 ;
        int gram = 0;
        int rest = grams % 454;
        pounds = grams / 454;
        ounces = rest / 28;
        rest = rest % 28;
        gram = rest;

        System.out.println("Pounds " + pounds + "ounces " + ounces + " grams " + gram);

        return new BritishWeightUnits(pounds, ounces, gram);
        }
    }
}
