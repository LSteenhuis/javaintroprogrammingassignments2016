/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Scanner;
import static jdk.nashorn.internal.objects.NativeMath.round;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {

    List<String> sortedAnimals = getSupportedAnimals();
    Animal an = new Animal();

    public static void main( String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
    
        anSim.start(args);
    }

    /**
     * Checks the given arguments and continues the program based on the given arguments.
     * @param args the name and age of different animals
     */
    private void start(final String[] args) {
        if (args.length == 0 || args.length % 2 != 0 || args[0].equalsIgnoreCase("help")) {
            System.out.println("Usage: java AnimalSimulator <Species age Species age ...>");
            System.out.println("Supported species (in alphabetical order):");
            int i = 1;
            for (String animals : sortedAnimals) {
                System.out.println(i + ": " + animals);
                i++;
            }

        } else {
            for (int i = 0; i < args.length; i = i + 2) {
                boolean valid = argumentChecker(args[i], args[i + 1]);
                if (valid == true) {
                    Integer age = Integer.parseInt(args[i + 1]);
                    String name = args[i].toString();
                    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");
                    if (name.equalsIgnoreCase("Elephant")) {
                        Elephant ele = new Elephant(age);
                        if (ele.cont == true) {
                            System.out.println("An Elephant of age " + age + " moving in " + ele.getMovementType() + " at " + oneDigit.format(ele.getSpeed()) + " km/h");
                        }
                    } else if (args[i].equalsIgnoreCase("Horse")) {
                        Horse hor = new Horse(age);
                        if (hor.cont == true) {
                            System.out.println("A Horse of age " + age + " moving in " + hor.getMovementType() + " at " + oneDigit.format(hor.getSpeed()) + " km/h");
                        }
                    } else if (args[i].equalsIgnoreCase("Mouse")) {
                        HouseMouse hm = new HouseMouse(age);
                        if (hm.cont == true) {
                            System.out.println("A Mouse of age " + age + " moving in " + hm.getMovementType() + " at " + oneDigit.format(hm.getSpeed()) + " km/h");
                        }
                    } else if (args[i].equalsIgnoreCase("Tortoise")) {
                        Tortoise tor = new Tortoise(age);
                        if (tor.cont == true) {
                            System.out.println("A Tortoise of age " + age + " moving in " + tor.getMovementType() + " at " + oneDigit.format(tor.getSpeed()) + " km/h");
                        }
                    }
                }
            }
        }
    }

    /**
     * returns all supported animals as List, alhabetically ordered.
     *
     * @return supportedAnimals the supported animals
     */
    public final List<String> getSupportedAnimals() {
        Animal animal = new Animal();
        List<String> sortedAnimals = animal.getSupportedAnimals();
        return sortedAnimals;
    }

    /**
     * Checks if there are illegal characters in the given arguments.
     * @param animal the name of the animal
     * @param age the age of the animal
     * @return boolean true if an illegal character has been found.
     */
    protected final boolean argumentChecker(final String animal, final String age) {
        for (String animals : sortedAnimals) {
            if (animal.equalsIgnoreCase(animals)) {
                try {
                    double doubleAge = Double.parseDouble(age);
                    return true;
                } catch (Exception e) {
                    System.out.println("Error: the following is not a correct number " + age + " . run with single parameter \"help\" to get a listing of available species. Please give new values");
                }
            }
        }
        System.out.println("Error: animal species " + animal + " is not known. run with single parameter \"help\" to get a listing of available species. Please give new values");
        return false;

    }



}
