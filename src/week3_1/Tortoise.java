/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Tortoise extends Animal {

    /**
     * The Tortoise object.
     *
     * @param age set age of the animal
     */
    public Tortoise(final Integer age) {
        double MaxAge = 190;
        double MaxSpeed = 0.30;
        String Movement = "crawl";
        setName("Tortoise");
        setMaxAge(MaxAge);
        setAge(age);
        setMaxSpeed(MaxSpeed);
        setMovement(Movement);
        
        setSpeed();

    }
}
