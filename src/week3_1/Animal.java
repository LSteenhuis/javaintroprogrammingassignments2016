/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {

    String name;
    String movement;
    double speed;
    double maxSpeed;
    double age;
    double maxAge;
    boolean cont = true;

    /**
     * returns the supported animals that can be used for the program
     * @return List String a list with the supported animals
     */
    public final List<String> getSupportedAnimals() {
        List<String> supportedAnimals = new ArrayList();
        supportedAnimals.add("Tortoise");
        supportedAnimals.add("Horse");
        supportedAnimals.add("Elephant");
        supportedAnimals.add("Mouse");

        Collections.sort(supportedAnimals);
        return supportedAnimals;
    }

    /**
     * returns the name of the animal.
     *
     * @return name the species name
     */
    public final String getName() {
        return this.name;
    }

    /**
     * returns the movement type.
     *
     * @return movementType the way the animal moves
     */
    public final String getMovementType() {
        return this.movement;
    }

//    /**
//     * returns the speed of this animal
//     *
//     * @return speed the speed of this animal
//     */
//    public double getSpeed() {
//        return this.speed;
//    }

    /**
     * Return the max age of this animal.
     *
     * @return maxAge the max age of this animal
     */
    public final double getMaxAge() {
        return this.maxAge;
    }

    /**
     * Returns the max speed of this animal.
     *
     * @return maxSpeed the max speed of this animal
     */
    public final double getMaxSpeed() {
        return this.maxSpeed;
    }

    /**
     * Sets the name of this animal.
     * @param name name
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the movement type of this animal.
     * @param movement the movement type of this animal
     */
    public final void setMovement(final String movement) {
        this.movement = movement;
    }

    /**
     * Sets the age of this animal.
     * @param age the age of the animal
     */
    public final void setAge(final double age) {
        if (age <= this.maxAge && age >= 0) {
            this.age = age;
        } else {
            System.out.println("Error: maximum age of " + this.name + " is " + this.maxAge + " years. Please give new values");
            this.cont = false;
        }
    }

    /**
     * Sets the age of this animal.
     * @param maxAge the max age of this animal
     */
    public final void setMaxAge(final double maxAge) {
        this.maxAge = maxAge;
    }

    /**
     * Sets the max speed of this animal.
     * @param maxSpeed the max speed of this animal
     */
    public final void setMaxSpeed(final double maxSpeed) {
        this.maxSpeed = maxSpeed;

    }

    /**
     * Sets the speed of this animal.
     */
    public final void setSpeed() {
        this.speed = this.maxSpeed * (0.50 + (0.50 * ((this.maxAge - this.age) / this.maxAge)));

    }
    /**
     * Gets the speed for the animal.
     * @return speed the speed of the animal
     */
    public final double getSpeed() {
        return this.speed;
    }

    public final boolean getCont() {
        return this.cont;
    }

}
