/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal {

    /**
     * The Horse object.
     *
     * @param age set age of the animal
     */
    public Horse(Integer age) {
        double MaxAge = 62;
        double MaxSpeed = 88.0;
        String Movement = "gallop";
        setName("Horse");
        setMaxAge(MaxAge);
        setMaxAge(MaxAge);
        setAge(age);
        setMaxSpeed(MaxSpeed);
        setMovement(Movement);
        
        setSpeed();
    }
}
