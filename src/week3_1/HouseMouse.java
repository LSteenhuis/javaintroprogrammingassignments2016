/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class HouseMouse extends Animal {

    /**
     * The HouseMouse object.
     *
     * @param age set age of the animal
     */
    public HouseMouse(final Integer age) {
        int MaxAge = 13;
        double MaxSpeed = 21.0;
        String Movement = "scurry";
        setName("Mouse");
        setMaxAge(MaxAge);
        setAge(age);
        setMaxSpeed(MaxSpeed);
        setMovement(Movement);
        
        setSpeed();

    }
}
