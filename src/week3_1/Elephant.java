/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class Elephant extends Animal {
    //private final Integer eleMaxAge = 86;

    /**
     * The Elephant object.
     * @param age set age of the animal
     */
    public Elephant(final Integer age) {
        double MaxAge = 86;
        double MaxSpeed = 40.0;
        String Movement = "thunder";
        setName("Elephant");
        setMaxAge(MaxAge);
        setAge(age);
        setMaxSpeed(MaxSpeed);
        setMovement(Movement);
        
        setSpeed();

    }
}




