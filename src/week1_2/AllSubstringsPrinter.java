/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package week1_2;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {

    /**
     * main method serves development purposes only.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("AGCGCT", false, false); //should print left truncated, left aligned
    }

    /**
     * will print all possible substrings according to arguments.
     *
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be truncated from the
     * left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be printed left-aligned (or
     * right-aligned)
     */
    public final void printAllSubstrings(
            final String stringToSubstring,
            final boolean leftTruncated,
            final boolean leftAligned) {
        if (leftTruncated == true && leftAligned == true) {
//            System.out.println(stringToSubstring);
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(stringToSubstring.substring(i));

            }
        } else if (leftTruncated == false && leftAligned == true) {
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(stringToSubstring.substring(0, stringToSubstring.length() - i));
            }
        } else if (leftTruncated == true && leftAligned == false) {
            StringBuilder sb = new StringBuilder(stringToSubstring);
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(sb.toString());
                sb.deleteCharAt(stringToSubstring.length() - 1);
                sb.insert(0, " ");
            }
        } else {
            StringBuilder sb = new StringBuilder(stringToSubstring);
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(sb.toString());
                sb.deleteCharAt(i);
                sb.insert(0, " ");

            }
        }
    }
}
