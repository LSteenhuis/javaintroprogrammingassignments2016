/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regexer class.
 * This is the main hub for all standard regex searches.
 * @author larssteenhuis
 */
public class Regexer {
    //initialisation of pattern and matcher
    Pattern pattern = Pattern.compile("");
    Matcher matcher = pattern.matcher("");
    
    /**
     * getAccno.
     * Searches line for the accension number
     * @param line the header of a fasta sequence
     * @return accno, accension number
     */
    public final String getAccno(String line){
        String accno = "";
        //pattern to find the accension number
        pattern = Pattern.compile("gi\\|[0-9]+");
        matcher = pattern.matcher(line);
        if (matcher.find()) {
            accno = matcher.group(0);
        }
    return accno;
    }
    
    /**
     * getProteinName.
     * searches line for protein name
     * @param line the header of a fasta file
     * @return proteinName the name of the found protein
     */
    public final String getProteinName(String line){
        String proteinName =  "";
        pattern = Pattern.compile("\\| (.*?)\\[");
        matcher = pattern.matcher(line);
        if (matcher.find()) {
            proteinName = matcher.group(0).substring(2, matcher.group(0).length() - 2);
        }else{
            //if the first regex fails this pattern will get the protein name.
            pattern = Pattern.compile("(\\w+ )");
            matcher = pattern.matcher(line);
            if (matcher.find()){
                proteinName = matcher.group(0);
            }

        }
        return proteinName;
    }

    /**
     * getOrganism.
     * searches the line for an organism, sets to NA if not found.
     * @param line header of sequence
     * @return organism the name of the organism
     */
    public final String getOrganism(String line){
        String organism = "";
        //pattern to find the organism, if it is not given it will
        //set the organism name to NA.
        pattern = Pattern.compile("\\[(.*?)\\]");
        matcher = pattern.matcher(line);
        if (matcher.find()) {
            organism = matcher.group(0).substring(1, matcher.group(0).length() - 1);
        }else{
            organism = "NA";
        }
        return organism;
    }
}
