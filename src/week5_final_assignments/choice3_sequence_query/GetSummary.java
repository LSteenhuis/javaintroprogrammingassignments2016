/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.commons.lang3.SystemUtils;

/**
 * GetSummary.
 * Creates an summary for the given file.
 * @author Lars
 */
public class GetSummary {

    /**
     *This function will report the summary of the given fasta file.
     * Uses a BufferedReader to iterate over the fasta file to count the numer
     * of sequences and calculates the average sequence length.
     * Prints the file name, number of sequences and average sequence length to 
     * the user.
     * @param fastaFile The path to the fasta file
     */
    public final void summary(final Path fastaFile) {
        int sequenceLength = 0;
        int numberOfSequences = 0;
        String type = "";
        String fastaPath = fastaFile.toString();
        String[] splitFasta;
        splitFasta = fastaPath.split("/");
        Charset charset = Charset.forName("US-ASCII");
        
        //Creates bufferedreader iterates of fastaFile
        try (BufferedReader reader = Files.newBufferedReader(fastaFile, charset)) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                //check if line is header
                if (line.startsWith(">")) {
                    numberOfSequences += 1;
                } else {
                    //adds the sequencelength to the variable
                    sequenceLength += line.length();
                    //checks wether aminoacid sequence is DNA or Protein
                    if (line.matches("^[ACGTacgt]+$")) {
                        type = "DNA";
                    } else {
                        type = "PROTEIN";
                    }
                }
            }
            //calculates average sequence length
            int avgSequenceLength = sequenceLength / numberOfSequences;
            System.out.println("File:                       " + splitFasta[splitFasta.length - 1]
                               + "\nSequence Types:             " + type
                               + "\nNumber of sequences:        " + numberOfSequences
                               + "\nAverage sequence length:    " + avgSequenceLength);
        //catches an IOException if the given fasta file does not exist
        } catch (IOException x) {
            System.err.format("IOException: given fastafile does not exist %n" + fastaFile + "\n");
        }
    }
}
