/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week5_final_assignments.choice3_sequence_query;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import static javafx.application.Platform.exit;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * SequenceQuery.
 * Main hub of the program
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class SequenceQuery {

    Path fastaFile;

    /**
     * starts the application
     *
     * @param args the command line arguments args
     * @throws org.apache.commons.cli.ParseException throws
     * @throws java.io.IOException throws
     */
    public static void main(final String[] args) throws ParseException, IOException {
        SequenceQuery mainObject = new SequenceQuery();
        mainObject.start(args);
    }

    /**
     * private constructor.
     */
    private SequenceQuery() {
    }

    /**
     * starts the application.
     *
     * @param args the arguments of the user
     * @throws IOException throws
     * @throws ParseException throws
     */
    private void start(final String[] args) throws ParseException, IOException {
        Options options = createOptions();       
        if (args.length != 0) {
            cliParser(options, args);
        } else {
            usage();
        }

    }

    /**
     * Shows the user the manual for the program
     */
    private void usage() {
        System.out.println("Usage: java sequenceQuery -input <INFILE> ...>");
        System.out.println(
                "-help Will show the usage\n"
                + "-summary Creates a textual summary of the parsed file: number of sequences, sequence type, average length.\n"
                + "-input <INFILE> -to_csv \";\" Generates a formatted csv listing with these columns ands the given character as separator:\n"
                + "-input <INFILE> --fetch_prosite <PROSITE PATTERN>  report all sequences that contain the Prosite pattern, with the location and actual sequence found.\n"
                + "     example of a prosite pattern 'H-x-H-x-H [VI]' this pattern will search for all possible patterns where x will be replaced by the letters inside the brackets."
                + "-input <INFILE> --find_regex <REGEX_PATTERN>  report all sequences that contain the Regular expression pattern, with the location and actual sequence found.\n"
                + "     example of a regex pattern 'H.H.H[VI]'this pattern will search for all possible patterns where . will be replaced by the letters inside the brackets"
                + "-input <INFILE> -find_organism <(PART OF) ORGANISM_NAME>  report all sequences having this wildcard string (a regex pattern) in the organism name\n"
                + "-input <INFILE> -find_description <WILDCARD-STRING>  report all sequences having this wildcard string (a regex pattern) in the description / sequence name\n"
                + "      example of a wildcard-string: [Hh]histidine");
    }

    /**
     * The initiator for the command line options.
     *
     * @return options object
     */
    private Options createOptions() {

        Options options = new Options();
        options.addOption("input", true, "input fasta file name");
        options.addOption("help", false, "show usage");
        options.addOption("summary", false, "Creates a textual summary of the parsed file: number of sequences, sequence type, average length");
        options.addOption("to_csv", true, "Generates a formatted csv listing with these columns ands the given character as separator");
        options.addOption("find_prosite", true, " report all sequences that contain the Prosite pattern, with the location and actual sequence found");
        options.addOption("find_regex", true, " report all sequences that contain the Regular expression pattern, with the location and actual sequence found");
        options.addOption("find_organism", true, "report all sequences having this wildcard string (a regex pattern) in the organism name");
        options.addOption("find_id", true, "report all sequences having this wildcard string (a regex pattern) in the description / sequence name");
        options.addOption("find_description", true, "report all sequences having this wildcard string (a regex pattern) in the description / sequence name");

        return options;

    }

    /**
     * The parser for the cli. Checks which commands are used and starts the
     * corresponding procedure.
     *
     * @param options Options object
     * @param args arguments of the user
     * @throws ParseException throws
     * @throws IOException throws
     */
    private void cliParser(final Options options, final String[] args) throws ParseException, IOException {
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            //Checks if input file has been genereated
            if (cmd.hasOption("input")) {
                //checks in inputfile exist.
                fastaFile = Paths.get(cmd.getOptionValue("input"));
                if (Files.exists(fastaFile)) {
                    //checks if option == help
                    if (cmd.hasOption("help")) {
                        usage();
                    // use summary option
                    } else if (cmd.hasOption("summary")) {
                        GetSummary gs = new GetSummary();
                        gs.summary(fastaFile);
                    //use generate csv file
                    } else if (cmd.hasOption("to_csv")) {
                        GenerateCsv gc = new GenerateCsv();
                        gc.generateCsv(fastaFile, cmd.getOptionValue("to_csv"));
                    //use find prosite    
                    } else if (cmd.hasOption("find_prosite")) {
                        RegexSearcher rs = new RegexSearcher();
                        rs.parseFileForRegex(fastaFile, cmd.getOptionValue("find_prosite"), true);
                    //use custom regex
                    } else if (cmd.hasOption("find_regex")) {
                        RegexSearcher rs = new RegexSearcher();
                        try {
                            if (cmd.getOptionValue("find_regex").contains(".") == true) {
                                rs.parseFileForRegex(fastaFile, cmd.getOptionValue("find_regex"), false);
                            } else {
                                throw new IllegalArgumentException("");
                            }
                        } catch (IllegalArgumentException e) {
                            System.err.println("Please use . as a seperator");
                        }
                    // find organism    
                    } else if (cmd.hasOption("find_organism")) {
                        RegexSearcher rs = new RegexSearcher();
                        rs.searchIdentifier(fastaFile, cmd.getOptionValue("find_organism"), "organism");
                    // use search identifier    
                    } else if (cmd.hasOption("find_id")) {
                        RegexSearcher rs = new RegexSearcher();
                        rs.searchIdentifier(fastaFile, cmd.getOptionValue("find_id"), "id");
                    //use find description
                    } else if (cmd.hasOption("find_description")) {
                        RegexSearcher rs = new RegexSearcher();
                        rs.searchIdentifier(fastaFile, cmd.getOptionValue("find_description"), "desc");

                    }

                } else {
                    System.out.println("File does not exist, please try again with another file name.");
                }
            }

        } catch (UnrecognizedOptionException e) {
            System.err.println("Unable to find the given argument " + Arrays.toString(args).split("-")[2].replace("]", "") + " please refer to the usage to see what functions are supported");
            usage();
        }

    }
}
