/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * RegexSearcher.
 * Searches the given file with a custom regex pattern.
 * @author Lars
 */
public class RegexSearcher {

    /**
     * global variable for the ASCII char set.
     */
    Charset charset = Charset.forName("US-ASCII");
    /**
     * global variable for the accession number.
     */
    String accno = "";
    /**
     * global variable for the name of the protein.
     */
    String name = "";
    /**
     * global variable for the name of the organism.
     */
    String organism = "";
    /**
     * global variable for the type of the sequence (DNA or PROTEIN).
     */
    String type = "";

    /**
     *
     */
    boolean foundSequences = false;

    /**
     * Find the prosite or a regex sequences in the fastafile.
     * Creates buffered reader to iterate of fastaFile.
     * gives sequence to findSequence with the amino acid sequence, regex pattern
     * and the desired mode.
     * When nothing has been found the program will relay that to the user.
     * @param fastaFile the given fasta file
     * @param ps the prosite or regex pattern
     * @param mode defines if it is a regex or prosite pattern.
     * @throws IOException thrown when fastaFile does not exist
     */
    public final void parseFileForRegex(final Path fastaFile, final String ps, final boolean mode) throws IOException {
        System.out.println("ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ");
        StringBuilder aminoAcidCode = new StringBuilder();
        boolean flag = false;
        String line = "";
        
        try (BufferedReader reader = Files.newBufferedReader(fastaFile, charset)) {
            while ((line = reader.readLine()) != null) {
                //check if line is a header
                if (line.startsWith(">")) {
                    if (flag == true) {
                        //passes finsequence the amino acide code, pattern and mode.
                        findSequence(aminoAcidCode.toString(), ps, mode);
                        aminoAcidCode = new StringBuilder();
                    }
                    //gives line to relayFinding
                    Regexer regex = new Regexer();
                    accno = regex.getAccno(line);
                    name = regex.getProteinName(line);
                    organism = regex.getOrganism(line);
                } else if (!"".equals(line)) {
                    flag = true;
                    //appens line to amino acid code
                    aminoAcidCode.append(line);
                    //check which type of sequence is provided.
                    if (line.matches("^[ACGTacgt]+$")) {
                        type = "DNA";
                    } else {
                        type = "PROTEIN";
                    }
                }
            }
            //checks wether items are found with the given pattern
            findSequence(aminoAcidCode.toString(), ps, mode);
            if (foundSequences == false) {
                System.out.println("Nothing has been found");
            }

        }
    }

    /**
     * This function will search if the query matches an identifier in the fasta
     * file.
     * Creates a buffered reader and searches each header for the identifier.
     * switches mode until the required one has been found.
     * creates a list with the found patterns.
     * @param fastaFile the fastafile
     * @param query the organism or id the user is looking for
     * @param mode organism / id
     * @throws IOException throws
     */
    public final void searchIdentifier(final Path fastaFile, final String query, final String mode) throws IOException {
        StringBuilder aminoAcidCode = new StringBuilder();
        boolean flag = false;
        HashMap listOfOrganismFound = new HashMap();
        
        try (BufferedReader reader = Files.newBufferedReader(fastaFile, charset)) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                //checks if line is a header
                if (line.startsWith(">")) {
                    if (flag == true) {
                        //put organism in the list
                        listOfOrganismFound.put(organism, aminoAcidCode);
                    }
                    flag = false;
                    organism = line;
                    //switch cases for the regex
                    switch (mode) {
                        case "organism":
                            Pattern pattern = Pattern.compile("(" + query.toLowerCase() + ")");
                            Matcher matcher = pattern.matcher(organism.toLowerCase());
                            if (matcher.find()) {
                                flag = true;
                            }
                            break;

                        case "id":
                            pattern = Pattern.compile("(" + query.replace("|", "\\|").toLowerCase() + ")");
                            matcher = pattern.matcher(organism.toLowerCase());
                            if (matcher.find()) {
                                flag = true;
                            }
                            break;

                        case "desc":
                            pattern = Pattern.compile(query);
                            matcher = pattern.matcher(organism);
                            if (matcher.find()) {
                                flag = true;
                            }
                            break;

                    }
                } else if (flag == true) {
                    aminoAcidCode.append(line);
                }
            }
            if (flag == true) {
                listOfOrganismFound.put(organism, aminoAcidCode);
            }
            //prints the found organism or none when nothing has been found.
            if (!listOfOrganismFound.isEmpty()) {
                Iterator it = listOfOrganismFound.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    System.out.println(pair.getKey() + "\n" + pair.getValue());
                }
            } else {
                System.out.println("Nothing has been found, please try a different pattern.");
            }
        }
    }

    /**
     * Checks if a regex or prosite sequence is found in the given fasta file.
     * Will display in the following manner:
     * ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ gi|21595364;FHIT;protein;Homo
     * sapiens;PROTEIN;94;HVHVHV
     *
     * @param sequence the sequence of the protein
     * @param ps the prosite pattern
     * @param prositeFlag looking for prosite or regular expression
     */
    protected final void findSequence(final String sequence, final String ps, final boolean prositeFlag) {

        Pattern pattern = Pattern.compile("\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(ps);
        if (matcher.find()) {
            String variablePattern = matcher.group(0);
            String prositePattern = ps.substring(0, ps.length() - matcher.group(0).length() - 1);
            if (prositeFlag == true) {
                String x = prositePattern.replaceAll("-", "");
                String s = x.replaceAll("x", variablePattern);
                String y = "(" + s + ")";
                pattern = Pattern.compile(y);
                matcher = pattern.matcher(sequence);
            } else {
                String x = prositePattern.replace(".", variablePattern);
                String y = "(" + x + ")";
                pattern = Pattern.compile(y);
                matcher = pattern.matcher(sequence);
            }
            if (matcher.find()) {
                foundSequences = true;
                System.out.println(accno + ";" + name + ";" + organism + ";" + type + ";" + matcher.start() + ";" + matcher.group(0));
            }
        }
    }
}
