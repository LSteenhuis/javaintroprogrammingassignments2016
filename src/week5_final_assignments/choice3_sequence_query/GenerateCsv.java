/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5_final_assignments.choice3_sequence_query;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static jdk.nashorn.internal.objects.ArrayBufferView.length;

/**
 * GenerateCsv.
 * Creates an csv file based on a given seperator.
 * @author Lars
 */
public class GenerateCsv {

    /**
     * accension number.
     */
    String accno = "";
    /**
     * name of the protein.
     */
    String name = "";
    /**
     * name of the organism.
     */
    String organism = "";
    /**
     * Type of nucleotides (DNA or PROTEIN).
     */
    String type = "";
    /**
     * length of the sequence.
     */
    int length = 0;
    /**
     * Molecular weight.
     */
    int mol_weight = 0;

    /**
     * amino acid weight.
     */
    double aminoAcidWeight = 0;

    /**
     * In this function the CSV file will be generated in the following style:
     * ACCNO;NAME;ORGANISM;TYPE;LENGHT;MOL_WEIGHT
     * gi|21595364;FHIT protein;Homo sapiens;PROTEIN;147;19487.8
     *
     * 
     * Regular expressions are used to get the corresponding data from the fasta file.
     * After each sequence in the fasta file, the data is printed to the command line.
     * @param fastaFile the fasta file the user has given
     * @param sep the seperator the user has given
     * @throws IOException an IOEXception
     */
    public final void generateCsv(final Path fastaFile, final String sep) throws IOException {
        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(fastaFile, charset)) {
            String line = "";
            //printing the header line with information of the format.
            System.out.println("ACCNO;NAME;ORGANISM;TYPE;LENGHT;MOL_WEIGHT");           
            while ((line = reader.readLine()) != null) {
                //Check if the line in the file is a newline.
                if (line != "\n" ){
                    //Check if the line has header information.
                    if (line.startsWith(">")) {
                        //check if length != 0 so that it won't try to print on the first header.
                        if (length != 0) {
                            //set decimal format
                            DecimalFormat oneDigit = new DecimalFormat("######0.0");
                            //print the comma seperated values
                            System.out.println(accno + sep + name + sep + organism + sep + type + sep + length + sep + oneDigit.format(aminoAcidWeight));
                        }                     
                        length = 0;
                        aminoAcidWeight = 0;
                        Regexer regex = new Regexer();
                        accno = regex.getAccno(line);
                        name = regex.getProteinName(line);
                        organism = regex.getOrganism(line);

                    } else {
                        //calls generateMolecurlarWeight on the line and appends 
                        //the length to the variable length.
                        calculateMolecurlarWeight(line);
                        length += line.length();
                        //checks which type of sequence is used
                        if (line.matches("^[ACGTacgt]+$")) {
                            type = "DNA";
                        } else {
                            type = "PROTEIN";
                        }
                        
                    }
                }

            }
        }

    }

    /**
     * Calculates the molecular weight of the amino acids string.
     * uses the HashMap from getAminoWeights.
     * for loops over aminoAcidSequence and retrieves the weight from every 
     * aminoacid from the hashmap and adds it to aminoAcidWeight.
     *
     * @param aminoAcidSequence sequence of amino acids
     */
    protected final void calculateMolecurlarWeight(final String aminoAcidSequence) {
        HashMap aminoWeightsHM = getAminoWeights();
        for (char ch : aminoAcidSequence.toCharArray()) {          
            if(ch != 'X'){
            String aminoWeightObject = aminoWeightsHM.get(String.valueOf(ch)).toString();
            double aminoWeightDouble = Double.parseDouble(aminoWeightObject);
            aminoAcidWeight = aminoAcidWeight + aminoWeightDouble;         
           }
        }
    }

    /**
     * Initiates the HashMap which contains the amino acid weights.
     *
     * @return HashMap with the weights of amino acids
     */
    private HashMap getAminoWeights() {
        HashMap aminoWeights = new HashMap();
        aminoWeights.put("A", 89.0935);
        aminoWeights.put("R", 174.2017);
        aminoWeights.put("N", 132.1184);
        aminoWeights.put("C", 121.1590);
        aminoWeights.put("Q", 146.1451);
        aminoWeights.put("G", 75.0669);
        aminoWeights.put("H", 155.1552);
        aminoWeights.put("I", 131.1736);
        aminoWeights.put("L", 131.1736);
        aminoWeights.put("K", 146.1882);
        aminoWeights.put("M", 149.2124);
        aminoWeights.put("F", 165.1900);
        aminoWeights.put("P", 115.1310);
        aminoWeights.put("S", 105.0930);
        aminoWeights.put("T", 119.1197);
        aminoWeights.put("W", 204.2262);
        aminoWeights.put("Y", 181.1894);
        aminoWeights.put("V", 117.1469);
        aminoWeights.put("D", 133.0375);
        aminoWeights.put("E", 147.1350);

        return aminoWeights;
    }
}
