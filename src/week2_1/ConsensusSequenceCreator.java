/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week2_1;

import com.sun.prism.impl.BufferUtil;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {

    /**
     * testing main.
     *
     * @param args arguments
     */
    public static void main(final String[] args) {
        String[] sequences = new String[5];
        sequences[0] = "CGAATA";
        sequences[1] = "CGAAAA";
        sequences[2] = "GGACTA";
        sequences[3] = "GGAACA";
        sequences[4] = "TGAACA";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     *
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public final String createConsensus(final String[] sequences, final boolean iupac) {
        StringBuilder iupacCode = new StringBuilder();

        for (int sequencePosition = 0; sequencePosition < sequences[1].length(); sequencePosition++) {
            StringBuilder bases = new StringBuilder();
            for (int charPossition = 0; charPossition < sequences.length; charPossition++) {
                if (bases.toString().indexOf(sequences[charPossition].charAt(sequencePosition)) < 0) {
                    bases.append(sequences[charPossition].charAt(sequencePosition));
                }
            }

            String strinSb = bases.toString();
            char[] basesArray = strinSb.toCharArray();
            Arrays.sort(basesArray);
            String sortedBases = new String(basesArray);

            if (iupac == true) {
                HashMap iupacTable = new HashMap();
                iupacTable.put("G", "G");
                iupacTable.put("A", "A");
                iupacTable.put("T", "T");
                iupacTable.put("C", "C");
                iupacTable.put("CGT", "B");
                iupacTable.put("AGT", "D");
                iupacTable.put("ACT", "H");
                iupacTable.put("AGT", "V");
                iupacTable.put("ACGT", "N");
                iupacTable.put("AT", "W");
                iupacTable.put("CG", "S");
                iupacTable.put("AC", "M");
                iupacTable.put("GT", "K");
                iupacTable.put("AG", "R");
                iupacTable.put("CG", "T");

                iupacCode.append(iupacTable.get(sortedBases));

            } else {
                if (sortedBases.length() == 1) {
                    iupacCode.append(sortedBases);
                } else {
                    StringBuilder bracket = new StringBuilder();
                    bracket.append("[");
                    bracket.append(sortedBases.charAt(0));
                    for (int i = 1; i < sortedBases.length(); i++) {
                        bracket.append("/");
                        bracket.append(sortedBases.charAt(i));
                    }
                    bracket.append("]");
                    iupacCode.append(bracket);
                }
            }
        }
        System.out.println(iupacCode);
        return iupacCode.toString();
    }
}
