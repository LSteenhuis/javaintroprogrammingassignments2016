/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week4_1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {

    private final String name;
    private final String accession;
    private String aminoAcidSequence;
    private GOannotation goAnnotation;
    private double molecularWeight;

    /**
     * constructs without GO annotation;
     *
     * @param name name
     * @param accession accession
     * @param aminoAcidSequence aas
     */
    public Protein(final String name, final String accession, final String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        calculateMolecurlarWeight();
        illegalCharCheck();
    }

    /**
     * construicts with main properties.
     *
     * @param name name
     * @param accession acc
     * @param aminoAcidSequence aas
     * @param goAnnotation go
     */
    public Protein(final String name, final String accession, final String aminoAcidSequence, final GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        illegalCharCheck();
        calculateMolecurlarWeight();
        this.goAnnotation = goAnnotation;
    }

    /**
     * Checks if the aminoAcidSequence contains illegal characters.
     *
     * @return boolean true if no illegal characters have been found.
     * @throws IllegalArgumentException throws when an illegal character has
     * been found.
     */
    private void illegalCharCheck() {
        if (aminoAcidSequence.length() > 0 && aminoAcidSequence.charAt(aminoAcidSequence.length() - 1) == '*' || aminoAcidSequence.charAt(aminoAcidSequence.length() - 1) == '.') {
            System.out.println("End of sequence character found, making substring.");
            aminoAcidSequence = aminoAcidSequence.substring(0, aminoAcidSequence.length() - 1);
        } else {
            if (!aminoAcidSequence.toUpperCase().matches("[ARNDCEQGHILKMFPSTWYV]+")) {
                throw new IllegalArgumentException("Illegal character has been found in "
                        + "the proteins amino acid sequence. Please only enter valid one letter abbrevations for the amino acids.");
            }
        }
    }

    @Override
    public final int compareTo(final Protein o) {
        return this.getName().compareTo(o.getName());
    }

    /**
     * provides a range of possible sorters, based on the type that is
     * requested.
     *
     * @param type type 
     * @return proteinSorter sort
     */
    public final static Comparator<Protein> getSorter(final SortingType type) {
        if (type != null) {
            switch (type) {
                case ACCESSION_NUMBER:
                    return new AccessionComparator();

                case GO_ANNOTATION:
                    return new GOComparator();

                case PROTEIN_WEIGHT:
                    return new WeightComparator();
            }
        } else {
            throw new IllegalArgumentException();
        }
        return null;

    }

    /**
     * Calculates the molecular weight of the amino acids string and sets it .
     * in a global variable.
     */
    protected final void calculateMolecurlarWeight() {
        HashMap aminoWeightsHM = getAminoWeights();
        DecimalFormat fourDigit = new DecimalFormat("#,###0.0000");
        double aminoAcidWeight;
        aminoAcidWeight = 0;
        for (char ch : aminoAcidSequence.toCharArray()) {
            String aminoWeightObject = aminoWeightsHM.get(String.valueOf(ch)).toString();
            double aminoWeightDouble = Double.parseDouble(aminoWeightObject);
            aminoAcidWeight = aminoAcidWeight + aminoWeightDouble;
        }

        molecularWeight = Double.parseDouble(fourDigit.format(aminoAcidWeight));
    }

    /**
     * Initiates the HashMap which contains the amino acid weights.
     * @return  HashMap with the weights of amino acids
     */
    private HashMap getAminoWeights() {
        HashMap aminoWeights = new HashMap();
        aminoWeights.put("A", 89.0935);
        aminoWeights.put("R", 174.2017);
        aminoWeights.put("N", 132.1184);
        aminoWeights.put("C", 121.1590);
        aminoWeights.put("Q", 146.1451);
        aminoWeights.put("G", 75.0669);
        aminoWeights.put("H", 155.1552);
        aminoWeights.put("I", 131.1736);
        aminoWeights.put("L", 131.1736);
        aminoWeights.put("K", 146.1882);
        aminoWeights.put("M", 149.2124);
        aminoWeights.put("F", 165.1900);
        aminoWeights.put("P", 115.1310);
        aminoWeights.put("S", 105.0930);
        aminoWeights.put("T", 119.1197);
        aminoWeights.put("W", 204.2262);
        aminoWeights.put("Y", 181.1894);
        aminoWeights.put("V", 117.1469);
        aminoWeights.put("D", 133.0375);
        aminoWeights.put("E", 147.1350);

        return aminoWeights;
    }

    /**
     *
     * @return name the name.
     */
    public final String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number.
     */
    public final String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence.
     */
    public final String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation.
     */
    public final GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    /**
     *
     * @return GO annotation.
     */
    public final double getMolecularWeight() {
        return molecularWeight;
    }

    @Override
    public final String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }

}
