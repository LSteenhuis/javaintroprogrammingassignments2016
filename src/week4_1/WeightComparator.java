/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4_1;

import java.util.Comparator;

/**
 *
 * @author Lars
 */
public class WeightComparator implements Comparator<Protein> {

    @Override
    public int compare(Protein first, Protein second) {
        if (first.getMolecularWeight()> second.getMolecularWeight()) {
            return -1;
        }
        if (first.getMolecularWeight()< second.getMolecularWeight()) {
            return 1;
        }
        return 0;
    }

}
