/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4_1;

import java.util.Comparator;

/**
 *
 * @author Lars
 */
public class GOComparator implements Comparator<Protein> {




    /**
     * Sorts Proteins based on their GO annotation.
     * It sorts in the following order:
     *                                  Biological process
     *                                  Cellular Component
     *                                  Molecular Function
     * Everything are alphabetically ascending
     * @param first the first Protein object
     * @param second the second Protein object
     * @return int the result for the compareTo.
     */
    @Override
    public final int compare(final  Protein first, final Protein second) {
        GOannotation gaA = first.getGoAnnotation();
        GOannotation gaB = second.getGoAnnotation();
        int n;
        n = gaA.getBiologicalProcess().compareToIgnoreCase(gaB.getBiologicalProcess());
        if (n != 0) {
            return n;
        }
        n = gaA.getCellularComponent().compareToIgnoreCase(gaB.getCellularComponent());
        if (n != 0) {
            return n;
        }
        return gaA.getMolecularFunction().compareToIgnoreCase(gaB.getMolecularFunction());

    }
}
