/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4_1;

import java.util.Comparator;

/**
 *
 * @author Lars
 */
public class AccessionComparator implements Comparator<Protein>{



    /**
     * Sorts a list of proteins alphabetically ascending based on their Accession number.
     * @param first first Protein object
     * @param second second Protein object
     * @return int result of the compareTo
     */
    @Override
    public final int compare(final Protein first, final Protein second) {
       return first.getAccession().compareToIgnoreCase(second.getAccession());
    }

}
