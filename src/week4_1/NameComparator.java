/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4_1;

import java.util.Comparator;

/**
 *
 * @author Lars
 */
public class NameComparator implements Comparator<Protein> {


    /**
     * Compares a list of Proteins based on their name in an ascending order.
     * @param first the first Protein object
     * @param second the second Protein object.
     * @return int the result of the compareTo
     */
    @Override
    public final int compare(final Protein first, final Protein second) {
       return first.getName().compareTo(second.getName());
    }

}
